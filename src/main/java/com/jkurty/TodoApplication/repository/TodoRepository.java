package com.jkurty.TodoApplication.repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.jkurty.TodoApplication.model.Todo;

public interface TodoRepository extends JpaRepository<Todo, Long> {

}
