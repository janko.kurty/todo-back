package com.jkurty.TodoApplication.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import com.jkurty.TodoApplication.exception.ResourceNotFoundException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jkurty.TodoApplication.model.Todo;
import com.jkurty.TodoApplication.repository.TodoRepository;

@RestController
@RequestMapping("/api")
public class TodoController {

	@Autowired
	TodoRepository todoRepository;

	// Get all todos
	@GetMapping("/todos")
	public List<Todo> getTodos() {
		return todoRepository.findAll();
	}

	// get a single todo
	@GetMapping("/todos/{id}")
	public Todo getTodoById(@PathVariable(value="id") Long todoId) {

		return todoRepository.findById(todoId)
					.orElseThrow(() -> new ResourceNotFoundException("Todo", "id", todoId));
	}

	// create a new todo
	@PostMapping("/todos")
	public Todo createTodo(@Valid @RequestBody Todo todo) {
		return todoRepository.save(todo);

	}
	//update existing todo
	@PutMapping("/todos/{id}")
	public Todo updateTodo(@PathVariable(value="id") Long todoId,@RequestBody Todo todoDetails) {
		Todo todo = todoRepository.findById(todoId)
					.orElseThrow(() -> new ResourceNotFoundException("Todo", "id", todoId));
		
		todo.setName(todoDetails.getName());
		todo.setDesc(todoDetails.getDesc());
		
		Todo updatedTodo = todoRepository.save(todo);
		
		return updatedTodo;
		
	}

	// delete a todo
	@DeleteMapping("/todos/{id}")
	public ResponseEntity<?> deleteTodo(@PathVariable(value = "id") Long todoId) {
		todoRepository.findById(todoId)
			.orElseThrow(() -> new ResourceNotFoundException("Todo", "id", todoId));
		
		todoRepository.deleteById(todoId);
		
		return ResponseEntity.ok().build();
		
	}

}
